
# charts

Helm charts for collecting metrics and sending them to InfluxDB or Prometheus. Add repository to Helm using the following command:

```bash
helm repo add infragravity https://infragravity.gitlab.io/charts
```

This repository includes the following charts:

* Sonar for Linux - supports Microsoft SQL Server
* Akumuli time series database

Steps for deployment on Kubernetes and Windows containers are described [here](http://www.infragravity.com/knowledge-base/)
