# Akumuli Helm Chart

This chart deployes [Akumuli](http://github.com/akumuli/akumuli) time series database to Kubernetes 1.8+
By default, it creates persistent volume for Akumuli storage.

Repository:

```bash
helm repo add infragravity https://infragravity.gitlab.io/charts
```

Examples:

```bash
helm install infragravity/akumuli --name=aku --set persistence.enabled=false
```

```bash
helm install infragravity/akumuli  --name aku --set persistence.nvolumes=1,persistence.volume_size='1Gb',persistence.size=1Gi,image.tag=0.7.50-skylake
```

For this repository, cloned locally:

```bash
helm install stable/akumuli --name=aku --set persistence.enabled=false
```

```bash
helm install stable/akumuli --name aku --set persistence.nvolumes=1,persistence.volume_size='1Gb',persistence.size=1Gi,image.tag=0.7.50-skylake
```

The values.yaml file can be also modified to specify resource limits and image tags.